# vaccine-finder

This project aims to create simple Lambda function deployments that query pharmacies for COVID-19 vaccine appointments and sends alerts via SNS topics.

## Overview

This project uses the Serverless Framework to deploy AWS Lambda functions that are scheduled using AWS Event Bridge events, similar to cron jobs. Each of these Lambda functions determine their parameters from environment variables. The initial implementation only targets a single pharmacy, Hy-Vee, but other pharmacies can be easily added as new functions in the `src` directory and configured for creation and execution in the `serverless.yml` file.

## Prequisites

### Serverless Framework
Assuming you have NPM installed, just run:
```bash
npm install -g serverless
```

### Python 3.8 or above
It is recommended to use PyEnv. [See here](https://github.com/pyenv/pyenv)

### Poetry
[See here](https://python-poetry.org/docs/)

## How-to

### Add a new pharmacy or provider to monitor
All code should be created in the `src` directory. Take a look at `src/hyvee.py` or `src/cvs.py` for an example. You'll notice that all the Hy-Vee file does is calls the Hy-Vee GraphQL API with some simple parameters and then parses the response to determine under which criteria to send an alert. The CVS file uses a very basic but more secure approach. Most pharmacies lack a true public API with proper documentation but most are easy to discover by monitoring API requests from their website in the browser or running their mobile apps with MITM Proxy.

### Configure a SNS topic
The easiest, although frowned upon by some, approach to establishing a new SNS topic is to just create it through the AWS console along with the subscriptions. This makes the confirmation flow simple. This assumes you are ok with a mix of IaC and manual configuration in your AWS account. Once you've created the topic, you'll need to configure the ARN in the `serverless.yml` file by creating both IAM permissions to publish as well as passing it as a paramter to your function in an environment variable. See the example in the `serverless.yml` file for more details on the specifics.

### Deployment
You could certainly get fancy and deploy with a CI/CD pipeline but the easiest way to start, assuming your AWS account allows it and you're ok with it, is to just simply run `sls deploy`. This assumes you have your AWS account creds set as your default creds in your system. [See here](https://boto3.amazonaws.com/v1/documentation/api/latest/guide/credentials.html) for more info.

## Contributing
Do you have a new pharmacy integration that you'd like to add? Please submit a PR with the new function in the `src` directory and and example configuration in the `serverless.yml` file.
