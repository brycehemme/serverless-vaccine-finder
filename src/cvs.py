import requests
from attrdict import AttrDict
import logging
import json
import os
import boto3

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

BASE_URL = "https://www.cvs.com/immunizations"

def process_covid_vaccine_locations(
    state_filter, topic_arn
):

    starting_url = BASE_URL + "/covid-19-vaccine"
    state_url = BASE_URL + "/covid-19-vaccine.vaccine-status.%s.json?vaccineinfo" % state_filter

    headers = {
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "referer": "https://www.cvs.com/immunizations/covid-19-vaccine"
    }

    s = requests.Session()
    s.headers.update(headers)

    available_locations = []

    s.get(starting_url)
    s.post("https://www.cvs.com/RETAGPV3/storedetails/V2/getstoredetails", data=json.dumps('{"request":{"header":{"apiKey":"a2ff75c6-2da7-4299-929d-d670d827ab4a","appName":"CVS_WEB","channelName":"WEB","deviceToken":"d9708df38d23192e","deviceType":"DESKTOP","responseFormat":"JSON","securityType":"apiKey","source":"CVS_WEB","lineOfBusiness":"RETAIL","type":"retlegget"}}}'))
    response = s.get(state_url)
    if response.status_code == 200:
        r = AttrDict(json.loads(response.content))
    else:
        log.critical(
            "find_covid_vaccine_locations(%s, %s) failed. Status Code: %s. Response Text: %s"
            % (state_filter, topic_arn, response.status_code, response.text)
        )
        raise Exception(
            "find_covid_vaccine_locations(%s, %s) failed. Status Code: %s. Response Text: %s"
            % (state_filter, topic_arn, response.status_code, response.text)
        )

    pharmacy_list = r.responsePayloadData.data[state_filter]

    for pharmacy in pharmacy_list:
        if pharmacy["status"] != "Fully Booked":
            available_locations.append(pharmacy["city"] + ", " + pharmacy["state"])

    if len(available_locations) > 0:
        client = boto3.client("sns")
        location_string = "; ".join(available_locations)
        message = (
            "The following CVS locations have COVID-19 vaccine availability: %s"
            % location_string
        )
        log.info("Sending message: %s" % message)

        response = client.publish(
            TargetArn=topic_arn,
            Message=json.dumps({"default": message, "sms": message, "email": message}),
            Subject="CVS locations with vaccine availability",
            MessageStructure="json",
        )
    else:
        log.info("No available locations found")


def main(event, context):
    state_filter = os.getenv("STATE_FILTER")
    topic_arn = os.getenv("TOPIC_ARN")

    process_covid_vaccine_locations(
        state_filter, topic_arn
    )


if __name__ == "__main__":
    # IA
    process_covid_vaccine_locations(
        "IA", "arn:aws:sns:us-east-1:919717081889:covid-alerts"
    )
