import requests
from attrdict import AttrDict
import logging
import json
import os
import boto3

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

GRAPHQL_URL = "https://www.hy-vee.com/my-pharmacy/api/graphql"


def process_covid_vaccine_locations(
    radius, latitude, longitude, state_filter, topic_arn
):
    data = {
        "operationName": "SearchPharmaciesNearPointWithCovidVaccineAvailability",
        "variables": {"radius": radius, "latitude": latitude, "longitude": longitude},
        "query": "query SearchPharmaciesNearPointWithCovidVaccineAvailability($latitude: Float!, $longitude: Float!, $radius: Int! = 10) {\n  searchPharmaciesNearPoint(latitude: $latitude, longitude: $longitude, radius: $radius) {\n    distance\n    location {\n      locationId\n      name\n      nickname\n      phoneNumber\n      businessCode\n      isCovidVaccineAvailable\n      covidVaccineEligibilityTerms\n      address {\n        line1\n        line2\n        city\n        state\n        zip\n        latitude\n        longitude\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n",
    }

    headers = {
        "content-type": "application/json",
        "apollographql-client-name": "my-pharmacy",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36",
    }

    available_locations = []

    response = requests.post(GRAPHQL_URL, data=json.dumps(data), headers=headers)
    if response.status_code == 200:
        r = AttrDict(json.loads(response.content))
    else:
        log.critical(
            "find_covid_vaccine_locations(%s, %s, %s) failed. Status Code: %s. Response Text: %s"
            % (radius, latitude, longitude, response.status_code, response.text)
        )
        raise Exception(
            "find_covid_vaccine_locations(%s, %s, %s) failed. Status Code: %s. Response Text: %s"
            % (radius, latitude, longitude, response.status_code, response.text)
        )
    if r.data and r.data.searchPharmaciesNearPoint:
        for pharmacy in r.data.searchPharmaciesNearPoint:
            if pharmacy.location:
                if pharmacy.location.isCovidVaccineAvailable:
                    if pharmacy.location.address.state == state_filter:
                        if pharmacy.location.nickname:
                            available_locations.append(pharmacy.location.nickname)
                        else:
                            available_locations.append(pharmacy.location.name)

    if len(available_locations) > 0:
        client = boto3.client("sns")
        location_string = ", ".join(available_locations)
        message = (
            "The following HyVee locations have COVID-19 vaccine availability: %s"
            % location_string
        )
        log.info("Sending message: %s" % message)

        response = client.publish(
            TargetArn=topic_arn,
            Message=json.dumps({"default": message, "sms": message, "email": message}),
            Subject="HyVee locations with vaccine availability",
            MessageStructure="json",
        )
    else:
        log.info("No available locations found")


def main(event, context):
    radius = int(os.getenv("RADIUS"))
    latitude = float(os.getenv("LATITUDE"))
    longitude = float(os.getenv("LONGITUDE"))
    state_filter = os.getenv("STATE_FILTER")
    topic_arn = os.getenv("TOPIC_ARN")

    process_covid_vaccine_locations(
        radius, latitude, longitude, state_filter, topic_arn
    )


if __name__ == "__main__":
    # Des Moines
    process_covid_vaccine_locations(
        300, 41.58681, -93.6228, "IA", "arn:aws:sns:us-east-1:919717081889:covid-alerts"
    )
